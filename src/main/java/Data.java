/**
 * Created by epakarb on 2017-01-16.
 */
public class Data {
    private static int IDCounter = 0;
    private int ID;
    private String name;
    private int secretData;

    private String dupa;

    public Data(String name, int secretData) {
        this.ID = IDCounter++;
        this.name = name;
        this.secretData = secretData;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public int getSecretData() {
        return secretData;
    }

    public void setName(String newName){
        name = newName;
    }

    public String getDupa() {
        return dupa;
    }

    public void setDupa(String dupa) {

        this.dupa = dupa;
    }
}
