package bridge;

import java.util.Arrays;

/**
 * Created by Kate on 2017-01-30.
 */
public class Microwave implements Cook{
    @Override
    public void cookMeal(String[] ingredients) {
        System.out.println("Cooking in a microvave, meal contains: " + Arrays.toString(ingredients));
    }
}
