package bridge;

/**
 * Created by Kate on 2017-01-30.
 */
public abstract class Meal {
    protected Cook cook;

    public Meal(Cook cook) {
        this.cook = cook;
    }

    abstract void cookMeal();
}
