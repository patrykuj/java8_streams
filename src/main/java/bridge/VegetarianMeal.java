package bridge;

/**
 * Created by Kate on 2017-01-30.
 */
public class VegetarianMeal extends Meal{
    String[] ingridiens;

    public VegetarianMeal(String[] ingridiens, Cook cook) {
        super(cook);
        this.ingridiens = ingridiens;
    }

    @Override
    void cookMeal() {
        cook.cookMeal(ingridiens);
    }
}
