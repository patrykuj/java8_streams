package bridge;

/**
 * Created by Kate on 2017-01-30.
 */
public interface Cook{
    void cookMeal(String[] ingredients);
}
