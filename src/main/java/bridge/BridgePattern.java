package bridge;

/**
 * Created by Kate on 2017-01-30.
 */
public class BridgePattern {
    public static void main(String... args) {
        Cooker cooker = new Cooker();
        VegetarianMeal kebsVege = new VegetarianMeal(new String[]{"tortilla", "tomato", "letuece", "cucumber"}, cooker);
        kebsVege.cookMeal();

        Microwave microwave = new Microwave();
        VegetarianMeal pizzaVege = new VegetarianMeal(new String[]{"olives", "chese", "tomato", "mushrooms"}, microwave);
        pizzaVege.cookMeal();
    }
}
