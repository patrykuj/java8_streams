package Creational.prototype;

import corpo.Building;
import enums.BuildingType;

/**
 * Created by epakarb on 2017-01-23.
 */
public class Client {
    public static void main(String... args) {
        BuildingCache.loadCache();

        Building hellHole = BuildingCache.getBuilding(BuildingType.SMALL);
        Building callCenter = BuildingCache.getBuilding(BuildingType.BIG);

        Building haven = BuildingCache.getBuilding(BuildingType.SMALL);

        callCenter.setName("Call center");
        hellHole.setName("Hell hole");
        haven.setName("Haven harbour");

        System.out.println(callCenter.getName());
        System.out.println(hellHole.getName());
        System.out.println(haven.getName());
    }
}
