package Creational.prototype;

import corpo.Building;
import corpo.CorpoBuilding;
import corpo.CorpoRoom;
import corpo.Room;
import enums.BuildingType;
import enums.RoomType;

import java.util.HashMap;

/**
 * Created by epakarb on 2017-01-23.
 */

public class BuildingCache {
    private static HashMap<BuildingType, Building> buildings = new HashMap<>();

    public static Building getBuilding(BuildingType type){
        Building b = buildings.get(type);
        return (Building) b.clone();
    }

    public static void loadCache(){
        Room[] roomSmall = new Room[BuildingType.SMALL.getRoomCapasity()];
        for(int i = 0; i < BuildingType.SMALL.getRoomCapasity(); i++){
            roomSmall[i] = new CorpoRoom(RoomType.SMALL.getCapasity());
        }

        Building small = new CorpoBuilding(roomSmall, BuildingType.SMALL);
        buildings.put(BuildingType.SMALL, small);

        Room[] roomBig = new Room[BuildingType.BIG.getRoomCapasity()];
        for(int i = 0; i < BuildingType.BIG.getRoomCapasity(); i++){
            roomBig[i] = new CorpoRoom(RoomType.BIG.getCapasity());
        }

        Building big = new CorpoBuilding(roomBig, BuildingType.BIG);
        buildings.put(BuildingType.BIG, big);
    }
}
