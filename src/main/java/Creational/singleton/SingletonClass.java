package Creational.singleton;

/**
 * Created by epakarb on 2017-01-25.
 */
public class SingletonClass {
    private static SingletonClass instance;

    private SingletonClass(){}

    public synchronized SingletonClass getInstance(){
        if(instance == null){
            instance = new SingletonClass();
        }
        return instance;
    }
}

//        If you have some resource that
//
//        (1) can only have a single instance, and
//
//        (2) you need to manage that single instance,
//
//        you need a Creational.singleton.
