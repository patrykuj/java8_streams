package Creational.factory;

import corpo.Employee;
import corpo.Supervisor;
import corpo.Worker;
import enums.EmployeeType;

/**
 * Created by epakarb on 2017-01-23.
 */
public class Factory {
    public Employee getEmployee(EmployeeType type, String name){
        if(type.equals(EmployeeType.MANAGER)){
            return new Supervisor(name);
        }
        else if(type.equals(EmployeeType.WORKER)){
            return new Worker(name);
        }else {
            return null;
        }
    }
}
