package Creational.factory;

import corpo.Employee;
import enums.EmployeeType;

/**
 * Created by epakarb on 2017-01-23.
 */
public class Client {
    static public void main(String... args) {
        Factory f = new Factory();
        Employee workerBob, workerSteve, manager;

        workerBob = f.getEmployee(EmployeeType.WORKER, "Bob");
        workerSteve = f.getEmployee(EmployeeType.WORKER, "Steve");
        manager = f.getEmployee(EmployeeType.MANAGER, "John");

        System.out.println(workerBob.sayHello());
        System.out.println(workerSteve.sayHello());
        System.out.println(manager.sayHello());
    }
}
