package Creational.builder;

import corpo.Corpo;
import corpo.Supervisor;
import corpo.Team;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by epakarb on 2017-01-23.
 */
public class CorpoBuilder {
    public Corpo prepareEmptyCorpo(String name) {
        Supervisor manager = new Supervisor("BOB");
        Team team = new Team("Buggers", new ArrayList<>(), manager);
        manager.addTeam(team);

        List<Team> teams = new ArrayList<>();
        teams.add(team);

        List<Supervisor> managers = new ArrayList<>();
        managers.add(manager);

        return new Corpo(teams, managers, name);
    }
}
