package Creational.builder;

import corpo.Corpo;
import corpo.Supervisor;

/**
 * Created by epakarb on 2017-01-23.
 */
public class Client {
    public static void main(String... args) {
        CorpoBuilder corpoBuilder = new CorpoBuilder();
        Corpo corpo = corpoBuilder.prepareEmptyCorpo("Push Button INC");

        System.out.println(corpo.getName());
        for(Supervisor s : corpo.getManagers()){
            System.out.println(s.getName());
        }
    }
}
