package enums;

/**
 * Created by epakarb on 2017-01-23.
 */
public enum EmployeeType {
    WORKER("worker"), MANAGER("manager");

    private String type;

    EmployeeType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
