package enums;

/**
 * Created by epakarb on 2017-01-23.
 */
public enum RoomType {
    BIG(8), SMALL(4);

    private int capasity;

    RoomType(int i) {

        capasity = i;
    }

    public int getCapasity() {
        return capasity;
    }
}
