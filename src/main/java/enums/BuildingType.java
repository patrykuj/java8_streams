package enums;

/**
 * Created by epakarb on 2017-01-23.
 */
public enum BuildingType {
    BIG(5), SMALL(3);

    private int roomCapasity;

    BuildingType(int roomCapasity) {
        this.roomCapasity = roomCapasity;
    }

    public int getRoomCapasity() {
        return roomCapasity;
    }
}
