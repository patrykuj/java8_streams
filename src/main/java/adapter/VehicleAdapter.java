package adapter;

/**
 * Created by Kate on 2017-01-30.
 */
public class VehicleAdapter implements BasicLocomotive{
    AdvancedLocomotive advancedLocomotive;

    public VehicleAdapter(String vehicleName) {
        if(vehicleName.equals("SpaceRocket")) {
            advancedLocomotive = new SpaceRocket();
        } else if(vehicleName.equals("Helicopter")){
            advancedLocomotive = new Helicopter();
        }
    }

    @Override
    public void go(String vehicleName) {
        if(vehicleName.equals("SpaceRocket")){
            advancedLocomotive.startShoutle(vehicleName);
        } else if(vehicleName.equals("Helicopter")){
            advancedLocomotive.fly(vehicleName);
        }
    }
}
