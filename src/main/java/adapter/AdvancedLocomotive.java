package adapter;

/**
 * Created by Kate on 2017-01-30.
 */
public interface AdvancedLocomotive {
    void fly(String vehicleName);
    void startShoutle(String vehicleName);
}
