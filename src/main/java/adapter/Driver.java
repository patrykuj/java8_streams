package adapter;

/**
 * Created by Kate on 2017-01-30.
 */
public class Driver implements BasicLocomotive{
    private VehicleAdapter vehicleAdapter;

    @Override
    public void go(String vehicleName) {
        if(vehicleName.equals("Bike")){
            System.out.println("riding bike");
        }else if(vehicleName.equals("SpaceRocket") || vehicleName.equals("Helicopter")){
            vehicleAdapter = new VehicleAdapter(vehicleName);
            vehicleAdapter.go(vehicleName);
        }else{
            System.out.println("I can not use " + vehicleName);
        }
    }
}
