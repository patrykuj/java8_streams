package adapter;

/**
 * Created by Kate on 2017-01-30.
 */
public class SpaceRocket implements AdvancedLocomotive{
    @Override
    public void fly(String vehicleName) {
    }

    @Override
    public void startShoutle(String vehicleName) {
        System.out.println("To stars and beyond with " + vehicleName);
    }
}
