package adapter;

/**
 * Created by Kate on 2017-01-30.
 */
public interface BasicLocomotive {
    void go(String vehicleName);
}
