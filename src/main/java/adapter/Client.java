package adapter;

/**
 * Created by Kate on 2017-01-30.
 */
public class Client {
    public static void main(String... args){
        Driver driver = new Driver();

        driver.go("Helicopter");
        driver.go("Bike");
        driver.go("SpaceRocket");
    }
}
