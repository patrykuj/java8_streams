package adapter;

/**
 * Created by Kate on 2017-01-30.
 */
public class Helicopter implements AdvancedLocomotive{
    @Override
    public void fly(String vehicleName) {
        System.out.println("Flying using " + vehicleName);
    }

    @Override
    public void startShoutle(String vehicleName) {

    }
}
