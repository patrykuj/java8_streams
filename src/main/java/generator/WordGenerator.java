package generator;

import java.util.Random;

/**
 * Created by epakarb on 2017-01-24.
 */
public class WordGenerator {
    static private String[] wordTable ={ "We need to socialize buy in from the key players ", "We need to consider sunsetting research areas that are high in volume but low in impact ",
                                        "Personally penetrate the customer vertically and horizontally to build strong, professional relationships",
            "We clearly don't do pit-stops, we just successfully change the tyres and swap out the engine while we're doing 200 mph around the track ", "Please work on your silent feedback "};

    public static String getInspireWords(){
        Random r = new Random();
        return wordTable[r.nextInt(wordTable.length)];
    }
}
