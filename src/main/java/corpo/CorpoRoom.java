package corpo;

/**
 * Created by epakarb on 2017-01-23.
 */
public class CorpoRoom implements Room {
    private int roomNO;
    private int capasity;


    public CorpoRoom(/*int roomNO,*/ int capasity) {
        //this.roomNO = roomNO;
        this.capasity = capasity;
    }

    public int getRoomNO() {
        return roomNO;
    }

    public int getCapasity() {
        return capasity;
    }

    public Object clone(){
        Object clone = null;
        try{
            clone = super.clone();
        }catch (CloneNotSupportedException e){
            e.printStackTrace();
        }
        return clone;
    }
}
