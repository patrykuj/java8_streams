package corpo;

import enums.EmployeeType;
import generator.WordGenerator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by epakarb on 2017-01-23.
 */
public class Supervisor implements Employee{
    private static int IDCounter = 0;
    private int ID;
    private String name;
    private Date startDate;
    private boolean isEmployed;
    private BigDecimal wage;
    private List<Team> teams;
    private EmployeeType position;

    public Supervisor(String name, Date startDate, BigDecimal wage, List<Team> teams) {
        this.name = name;
        this.startDate = startDate;
        this.wage = wage;
        this.isEmployed = true;
        IDCounter++;
        ID = IDCounter;
        this.teams = teams;
        this.position = EmployeeType.MANAGER;
    }

    public Supervisor(String name) {
        this.name = name;
        this.teams = new ArrayList<>();
        this.position = EmployeeType.MANAGER;
    }

    public Worker fireRandom(){
        Random r = new Random();
        r.nextInt();
        return EmployeeDB.workersList.remove(r.nextInt(EmployeeDB.workersList.size()));
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public void addTeam(Team team){
        this.teams.add(team);
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public boolean isEmployed() {
        return isEmployed;
    }

    @Override
    public BigDecimal getWage() {
        return wage;
    }

    @Override
    public BigDecimal giveRaise(BigDecimal raiseValue) {
        wage = wage.add(raiseValue);
        return wage;
    }

    @Override
    public int getID() {
        return ID;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public EmployeeType getPosition() {
        return position;
    }

    @Override
    public String sayHello() {
        return "Sorry I am late for 'MITING' just remember: " + inspire();
    }

    String inspire(){
        return WordGenerator.getInspireWords();
    }
}
