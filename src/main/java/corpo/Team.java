package corpo;

import java.util.Arrays;
import java.util.List;

/**
 * Created by epakarb on 2017-01-23.
 */
public class Team {
    private Supervisor manager;
    private List<Team> teamMembers;
    private final String teamName;

    public Team(String name, List<Team> teamMembers, Supervisor manager){
        teamName = name;
        this.teamMembers = teamMembers;
        this.manager = manager;
    }

    void addWorker(Team... newMembers){
        teamMembers.addAll(Arrays.asList(newMembers));
    }

    void addWorker(List<Team> newMembers){
        teamMembers.addAll(newMembers);
    }

    public Supervisor getManager() {
        return manager;
    }

    public List<Team> getTeamMembers() {
        return teamMembers;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setManager(Supervisor manager) {
        this.manager = manager;
    }

    public void setTeamMembers(List<Team> teamMembers) {
        this.teamMembers = teamMembers;
    }
}
