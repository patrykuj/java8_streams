package corpo;

import enums.BuildingType;

import java.util.List;
import java.util.Objects;

/**
 * Created by epakarb on 2017-01-23.
 */
public class CorpoBuilding implements Building{
    Room [] rooms;
    BuildingType buildingType;
    String name;

    public CorpoBuilding(Room[] rooms, BuildingType bt) {
        rooms = new Room[bt.getRoomCapasity()];
        buildingType = bt;
        this.rooms = rooms;
        name = "???";
    }

    public Room[] getRooms() {
        return rooms;
    }

    @Override
    public Object clone() {
        Object o = null;
        try{
            o = super.clone();
        } catch(CloneNotSupportedException e){
            e.printStackTrace();
        }
        return o;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
