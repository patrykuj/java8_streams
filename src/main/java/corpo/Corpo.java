package corpo;

import java.util.List;

/**
 * Created by epakarb on 2017-01-23.
 */
public class Corpo {
    private List<Team> teams;
    private List<Supervisor> managers;
    private final String name;

    public Corpo(List<Team> teams, List<Supervisor> managers, String name) {
        this.teams = teams;

        this.managers = managers;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public void setManagers(List<Supervisor> managers) {
        this.managers = managers;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public List<Supervisor> getManagers() {
        return managers;
    }
}
