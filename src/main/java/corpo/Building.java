package corpo;

/**
 * Created by epakarb on 2017-01-23.
 */
public interface Building extends Cloneable {
    Object clone();
    void setName(String name);
    String getName();
}
