package corpo;

import enums.EmployeeType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by epakarb on 2017-01-23.
 */
public interface Employee {
    Date getStartDate();
    boolean isEmployed();
    BigDecimal getWage();
    BigDecimal giveRaise(BigDecimal raiseValue);
    int getID();
    String getName();
    EmployeeType getPosition();
    String sayHello();
}
