package corpo;

import enums.EmployeeType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by epakarb on 2017-01-23.
 */
public class Worker implements Employee {
    private static int IDCounter = 0;
    private int ID;
    private String name;
    private Date startDate;
    private boolean isEmployed;
    private BigDecimal wage;
    private String teamName;
    private EmployeeType position;

    public Worker(String name, Date startDate, BigDecimal wage) {
        this.name = name;
        this.startDate = startDate;
        this.wage = wage;
        IDCounter++;
        this.ID = IDCounter;
        this.isEmployed = true;
        this.position = EmployeeType.WORKER;
    }

    public Worker(String name) {
        this.name = name;
        this.position = EmployeeType.WORKER;
        EmployeeDB.workersList.add(this);
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    public void setTeam(String teamName){
        this.teamName = teamName;
    }

    @Override
    public boolean isEmployed() {
        return isEmployed;
    }

    @Override
    public BigDecimal getWage() {
        return wage;
    }

    @Override
    public BigDecimal giveRaise(BigDecimal raiseValue) {
        wage = wage.add(raiseValue);
        return wage;
    }

    @Override
    public int getID() {
        return ID;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public EmployeeType getPosition() {
        return position;
    }

    @Override
    public String sayHello() {
        return "Hello, my name is: " + name + " and I am working here as: " + position;
    }
}
