import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

public class Main {

    public static void main(String... args){
        List<Data> dataList = new ArrayList<>();
        Data[] data = {new Data("ada", 5), new Data("Kumil", 10), new Data("Stefan", 9),
                new Data("Bill", 14), new Data("Jan", 1), new Data("Jan", 1)};
        dataList = stream(data).collect(Collectors.toList());
        for(Data d : dataList){
            System.out.println(d.getName());
        }

        Arrays.stream(data).forEach(data1 -> data1.setDupa("Dupa"));

        for(Data d : dataList){
            System.out.println(d.getDupa());
        }

        //Arrays.stream(data)
        Map<Integer, Data> dataCopy = dataList.stream().collect(Collectors.toMap(data1 -> data1.getID(), data1 -> data1));

        for(Integer i : dataCopy.keySet()){
            System.out.println(dataCopy.get(i).getName());
        }

        List<Data> filter = dataList.stream().filter(data1 -> data1.getSecretData() > 6).collect(Collectors.toList());
        for(Data d : filter){
            System.out.println(d.getSecretData());
        }

        dataList.stream().distinct().forEach(data1 ->{
            if(data1.getSecretData() < 10){
                data1.setName("maluszek");
            }
        });

        for(Data d : dataList){
            System.out.println(d.getName());
        }

        Map mapka = new TreeMap();
//        Set setcik = new
    }
}
